import os
import sys
import urllib2
import json
import graphlab as gl
import time
import rpy2.robjects as robjects
from rpy2.robjects.packages import importr

##########################################
### Graphlab TopicModelling code
### Read the dictionary and feed in as data frame
### Remove Stop words and rt tag
### AliasLDA model - alpha = 50/numberoftopics, Beta = 0.01
##########################################

def topicModelling(dictPath="../data/new_data/subtweets/userFinalDictionary",meth="cgs", oldModel="",usePriorKnow=False, nit = 10):
	
	userTweets = dict()
	with open(dictPath,'r') as r_file:
		userTweets = json.load(r_file)

	users = userTweets.keys()
	tweets = userTweets.values()

	gframe = gl.SFrame({'user':users,'tweets':tweets})

	#Create Bag of Words
	tweets = gl.text_analytics.count_words(gframe['tweets'])
	#Remove Stop Words
	stopwords = gl.text_analytics.stopwords() | {'rt'} | {'RT'} | {'rT'} | {'Rt'} | {'http'} | {'500px'} |{'mytweetseparatordelimitor'} | {'<<'}
	tweets = tweets.dict_trim_by_keys(stopwords, exclude=True)


	timeStamp = str(time.time()%100000)
	####################
	#Parameters
	####################
	ntp = 20 #number of topics
	beta = 0.1
	nWords = 20
	####################
	wordCloudPDF = 'results/data/'+timeStamp+'_wordCloud.pdf'


	if oldModel!="":
		print("=================================")
		print("Ignoring Model Training")
		print("=================================")
		model = gl.load_model(oldModel)
		timeStamp = oldModel.split('/')
		timeStamp = timeStamp[len(timeStamp)-1].split("_")[0]

		wordCloudPDF = 'results/data/'+timeStamp+'_wordCloud.pdf'
	else:
		# Seeding Model with prior knowledge
		# stemming topic
		# associations = gl.SFrame({'word':['vaccin','vaccinedeb','hearthiswel','vaccinescauseaut','cdcfraudexpos','cdccoverup','vaccineeduc','saynotovaccin','parentsdothework','cdcwhistleblow','vaxtruth'],'topic': [0,0,0,0,0,0,0,0,0,0,0]})
		
		# Lemmatization topic
		# Learn topic model
		# alias or cgs
		if usePriorKnow:
			associations = gl.SFrame({'word':['vaccinedebate','hearthiswell','cdcfraud','vaccinescauseautism','cdcfraudexposed','cdccoverup','cdcwhistleblower','parentsdothework','saynotovaccines','vaccineeducated','vaxtruth'],'topic': [0,0,0,0,0,0,0,0,0,0,0]})
			model = gl.topic_model.create(tweets,method=meth,beta=beta,num_topics=ntp,num_iterations=nit,associations=associations)
		else:
			model = gl.topic_model.create(tweets,method=meth,beta=beta,num_topics=ntp,num_iterations=nit)

		print(model.get_topics())
		print(model)
		model.save('results/models/'+timeStamp+'_lastModel')

		########################
		## Write Topics to a File
		########################
		with open('results/data/'+timeStamp+'_topicsGenerated.txt','w') as w_file:
			index = 0
			while index < ntp:
				topic = model.get_topics([index],num_words=10)
				for line in topic:
					w_file.write(str(line['topic'])+"\t"+line['word']+"\t"+str(line['score'])+"\n")
				index+=1

	#########################
	#########################
	#########################
	print('Generating Collapsible Indented Tree')
	jsn = dict()
	jsn['name'] = 'Topics'
	jsn['children'] = []
	
	index = 0
	while index < ntp:
		topic = model.get_topics([index],num_words=10)
		tmp = dict()
		tmp['name'] = 'topic_'+str(index)
		tmp['children'] = []
		for line in topic:
			tmp['children'].append({'name':line['word'],'size':line['score']*10000})
		jsn['children'].append(tmp)
		index+=1

	with open('results/data/'+timeStamp+'_CIT.json','w') as w_file:
		json.dump(jsn,w_file)

	print('Finished Collapsible Indented Tree')
	##############################
	##############################
	## Generate Word Cloud using R
	##############################
	##############################

	importr('RColorBrewer')
	importr('wordcloud')
	pdf = robjects.r['pdf']
	pdf(wordCloudPDF)

	# scale = robjects.IntVector([nWords,1])
	scale = robjects.IntVector([8,1])
	wordcloud = robjects.r['wordcloud']
	brewer = robjects.r['brewer.pal']

	for n in range(0,ntp):
		topics = model.get_topics(topic_ids=[n],num_words=nWords)
		words = robjects.StrVector(topics['word'])
		score = robjects.FloatVector(topics['score'])
		# wordcloud(words,score,scale = scale,rot_per = 0,colors = brewer(9,"Blues"))
		wordcloud(words,score,scale = scale,rot_per = 0,colors = brewer(8, "Dark2"), use_r_layout=False, random_order=False)

	dev = robjects.r['dev.off']
	dev()

	##############################
	##############################



	#################################
	# User Profile Topic Predicting
	#################################

	# Get User - topic Probabilities
	pred = model.predict(tweets,output_type='probability')

	####
	## Print this to a file
	####
	with open('results/data/'+timeStamp+'_userTopicPrediction.txt','w') as w_file:
		i = 0
		while i < len(pred):
			w_file.write(str(users[i])+' '+ ' '.join(map(str,list(pred[i])))+"\n")
			i+=1

	# Get User - Topmost Topic
	topic_pred = model.predict(tweets)
	pred = pred.unpack()

	print()
	print("Running GraphLab K Means++")
	# Run K-Means++ for clustering users based on Probabilities
	kmodel = gl.kmeans.create(pred, num_clusters=ntp, max_iterations=100)

	

	with open('results/data/'+timeStamp+'_userCluster.txt','w') as w_file:
		i = 0
		topics = []
		for i in range(0,ntp):
			topics.append(list(model.get_topics([i],output_type='topic_words',num_words=10,)['words'][0]))

		while i < len(kmodel['cluster_id']):
			w_file.write(str(users[i])+"\t"+str(kmodel['cluster_id'][i]['cluster_id'])+"\t"+str(topic_pred[i])+"\t"+str(topics[topic_pred[i]])+"\n")
			i+=1

	kmodel.save('results/models/'+timeStamp+'_lastKModel')
	###################################


	
	###################################
	## R Code
	## Generate K Means PCA plot for user cluster
	###################################
	print("Running R K Means")
	print("Print Plot using R....")
	importr('fpc')
	kmeans = robjects.r['kmeans']
	plotcluster = robjects.r['plotcluster']
	pdf = robjects.r['pdf']
	
	rdf = robjects.DataFrame(pred[0])
	for ob in pred[1:]:
		rdf = rdf.rbind(robjects.DataFrame(ob))
	fit=kmeans(rdf,ntp)
	pdf('./results/data/'+timeStamp+'_UserClusterPlot.pdf')
	plotcluster(rdf,fit[0])
	dev = robjects.r['dev.off']
	dev()


def main(args):

	oldModel = ""
	if "-u" in args:
		index = args.index("-u")
		oldModel = args[index+1]
		del args[index]
		del args[index]

	method = "cgs"
	if "-m" in args:
		index = args.index("-m")
		method = args[index+1]
		del args[index]
		del args[index]

	itr = 10
	if "-i" in args:
		index = args.index("-i")
		itr = args[index+1]
		del args[index]
		del args[index]

	usePriorKnow = False
	if "-p" in args:
		args.remove('-p')
		usePriorKnow = True

	if len(args)>1:
		topicModelling(dictPath=args[1],oldModel=oldModel,usePriorKnow=usePriorKnow, meth = method, nit = itr)
	else:
		topicModelling(oldModel=oldModel,usePriorKnow=usePriorKnow, meth = method, nit = itr)


if __name__ == '__main__':
	main(sys.argv)