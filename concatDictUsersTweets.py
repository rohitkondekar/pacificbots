import multiprocessing
import os
import sys
import time
import re
import json
import urllib2
import logging
import os
import HTMLParser
import glob
import string
import unicodedata
from nltk.stem.snowball import SnowballStemmer
from nltk.stem import WordNetLemmatizer

#########################
## Concat small user dictionaries after filtering tweets
#########################
def concatDict(path,oldDictionary=""):
	userDict = dict()
	shortword = re.compile(r'\W*\b\w{1,3}\b')
	lemmatizer = WordNetLemmatizer()

	# stemmer = SnowballStemmer("english")

	#########################
	#If you have old data which you have to concatenate
	#########################
	if oldDictionary!="":
		with open(oldDictionary,'r') as r_file:
			userDict = json.load(r_file)

	#########################
	#Read Indiviual Dictionaries and concatenate each user tweets
	#########################
	files = glob.glob(path+'sub*_output')
	for f in files:
		with open(f,'r') as r_file:
			print(f)
			newDict = json.load(r_file)
			keys = userDict.viewkeys() | newDict.viewkeys()
			userDict = {k : userDict.get(k, '') +" "+ newDict.get(k, '') for k in keys}

	print('Filtering the tweets and removing non-latin characters')
	
	#########################
	#Filter the tweets
	#########################
	with open(path+'userTweets.txt','w') as w_file:
		for key in userDict:
			tweet = userDict[key]
			#Remove non-latin characters
			tweet = re.sub(ur'[^#\x00-\x7F\x80-\xFF\u0100-\u017F\u0180-\u024F\u1E00-\u1EFF]', u'', tweet)
			#Remove words starting with @
			tweet = re.sub('@[^ ]*','',tweet)
			#Remove punctuations
			puncs = string.punctuation
			puncs = re.sub('#','',puncs)
			tweet = tweet.translate({ord(k): None for k in puncs})
			#lowercase everything
			tweet = tweet.lower()
			#remove Short words
			tweet = shortword.sub('', tweet)
			tweet = re.sub(' # ','',tweet)

			#Lemmatization:
			new_tweet = ""
			try:
				for word in tweet.split():

					if isinstance(word,unicode):
						word = unicodedata.normalize('NFKD', word).encode('ascii','ignore')
					
					# word = stemmer.stem(word)
					word = lemmatizer.lemmatize(word)

					if isinstance(word,unicode):
						word = unicodedata.normalize('NFKD', word).encode('ascii','ignore')

					new_tweet+=" "+ str(word)
			except Exception as e:
				print(e)
				new_tweet = tweet

			w_file.write(key+"\t"+new_tweet+"\n")
			userDict[key] = new_tweet

	print("number of users: "+str(len(userDict.keys())))
	
	with open(path+'userFinalDictionary','w') as w_file:
		json.dump(userDict,w_file)

def main(args):
	if len(args)==2:
		concatDict(args[0],args[1])
	elif len(args)==3:
		concatDict(args[0],args[1],args[2])
	else:
		concatDict(args[0])

if __name__ == '__main__':
	main(sys.argv[1:])





