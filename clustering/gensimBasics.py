from gensim import corpora, models, similarities
import json, logging, gensim

inpF = "/home/mahesh_t/tweetAnalyzer/data/newdata/userFinalDictionary"
inpDir = "/home/mahesh_t/tweetAnalyzer/data/newdata/"
outF = "/home/mahesh_t/tweetAnalyzer/data/newdata_log"

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
topKTopics = 20

## Utilize user profile, userId to all tweet mapping to generate bow_corpus
rawData = open(inpF).read()
D = dict(json.loads(rawData))
noUsers = len(D)
tweets = []
stoplist = set('for a of the and to in with that this your from they will have what like dont about the http when \
				just most here there is an or also it'.split())
for k in D.keys():
	v = D[k]
	time_tweet = v.split('mytimedelimited')[1:]
	time = time_tweet[0::2]
	tweet = time_tweet[1::2] #All tweets of the user
	tweets.append(tweet)

# remove common words and tokenize #NOTE : tweet is aggregated tweet content of user
texts = [ sum([[ word for word in stweet.lower().split() if word not in stoplist ] for stweet in tweet ],[]) for tweet in tweets]
# # remove words that appear only once
# all_tokens = sum(texts, []) #Taking substantial time

# users = []
# for tweet in tweets:
# 	user = []
# 	for stweet in tweet:
# 		for word in stweet.split().strip().lower():
# 			if word not in stoplist:
# 				user.append(word)
# 	users.append(users)

# tokens_once = set(word for word in set(all_tokens) if all_tokens.count(word) == 1)
# texts = [[word for word in text if word not in tokens_once] for text in texts]

## Build model using bow vectors across all documents
dictionary = corpora.Dictionary(texts)
bow_corpus = [dictionary.doc2bow(text) for text in texts]
ldaM = models.ldamodel.LdaModel(bow_corpus, id2word=dictionary, num_topics=50, passes=50)
ldaM.save(outF)

#open(outF,'w').write(ldaM(topKTopics))

## size is degrees of freedom of training algorithm, 
# # sentences = [['first', 'sentence'], ['second', 'sentence']]
# class MySentences(object):
#     def __init__(self, dirname):
#         self.dirname = dirname
#     def __iter__(self):
#         for fname in os.listdir(self.dirname):
#             for line in open(os.path.join(self.dirname, fname)):
#                 yield line.split()
# sentences = MySentences(inpDir) # a memory-friendly iterator
# model = gensim.models.Word2Vec(sentences, min_count=10, size=200, workers=4)
# model = models.ldamodel.LdaModel(bow_corpus, id2word=dictionary, num_topics=100)

# ## using model
# #model.most_similar(positive=['woman', 'king'], negative=['man'], topn=1)
# #model.doesnt_match("breakfast cereal dinner lunch".split())
# #model.similarity('woman', 'man')
# #model['computer']
# #model[doc_bow]
