import numpy as np
import collections
import sklearn.cluster as skc
import sklearn.decomposition as skd
from sklearn.feature_extraction.text import TfidfTransformer as tfIdfT
import pylab as pl
from pandas import *
from gensim import corpora, models, similarities
import json, logging, gensim
import pprint

botFile = './data/bots.txt'
topicWordF = './data/newdata/92462.3410039_topicsGenerated.txt'
userTopicF = './data/newdata/92462.3410039_userTopicPrediction.txt'
tweetAggInpF = "/home/mahesh_t/tweetAnalyzer/data/newdata/userFinalDictionary"
userWordFile = "/home/mahesh_t/tweetAnalyzer/data/userWord.txt"

def get_topic_word_distr_rohit():
	dataByLine = open(topicWordF).read().split('\n')[:-1]
	data = map(lambda x : x.split('\t'),dataByLine)
	topicWord = collections.defaultdict(dict)
	for line in data:
		topicWord[line[0]][line[1]] = float(line[2])
	return topicWord

def get_user_topic_rohit():
	dataByLine = open(userTopicF).read().split('\n')[:-1]
	data = map(lambda x : x.split(' '),dataByLine)
	userTopic = collections.defaultdict(dict)
	for line in data:
		userTopic[line[0]] = map(lambda x : float(x), line[1:])
	return userTopic

def get_bots():
	return open(botFile).read().split('\n')

def get_user_word_rohit():
	rawData = open(tweetAggInpF).read()
	D = dict(json.loads(rawData))
	noUsers = len(D)
	tweets = []
	stoplist_unigram = set('for a of the and to in with that this your from they will have what like dont about the http when \
					just most here there is an or also it rt RT rT Rt http 500px'.split())
	for k in D.keys():
		v = D[k]
		time_tweet = v.split('mytimedelimited')[1:]
		time = time_tweet[0::2]
		tweet = time_tweet[1::2] #All tweets of the user
		tweets.append(tweet)	
	#remove common words and tokenize #NOTE : tweet is aggregated tweet content of user
	userWords = {}
	texts = [ sum([[ word for word in stweet.lower().split() if word not in stoplist_unigram ] for stweet in tweet ],[]) for tweet in tweets]
	dictionary = corpora.Dictionary(texts)	
	del texts, tweets
	for k in D.keys():
		v = D[k]
		time_tweet = v.split('mytimedelimited')[1:]
		time = time_tweet[0::2]
		tweet = time_tweet[1::2] 
		tokens = sum([[ word for word in stweet.lower().split() if word not in stoplist_unigram ]
			for stweet in tweet ],[])
		userWords[k] = dictionary.doc2bow(tokens)
	#pprint.pprint(globals())
	#pprint.pprint(locals())
	with open(userWordFile, 'w') as outfile:
		json.dump(userWords, outfile)
	return userWords

def cluster_data(matrix):
	dim1 = len(matrix)
	dim2 = len(matrix[0])
	#print dim1, dim2
	num_clusters = min(dim1,dim2)
	if num_clusters==dim1:
		model = skc.KMeans(n_clusters=num_clusters, init=matrix, precompute_distances=True, n_jobs=1)
	else:
		model = skc.KMeans(n_clusters=num_clusters, precompute_distances=True, n_jobs=1)
	clusterId = model.fit_predict(matrix)
	return clusterId, model.cluster_centers_

def plot_clusters_with_newdatapoints(clusters,clusterId,orgMatrix,dataPoint,lbl):
	u, s, v = np.linalg.svd(clusters)
	projectionVec = v[:,0:2]
	#idx = (-np.array(abs(val))).argsort()[:2]
	projectedData = np.dot(clusters,projectionVec)	
	pl.title('Plotting '+lbl)	
	s = pl.scatter(projectedData[:,0], projectedData[:,1]) #, s=50, c = (1.0*clusterId)/len(clusters[0]))
	#pl.gcf().colorbar(s).set_label('ColorBar')
	for i in range(len(projectedData[:,0])):
		pl.annotate(str(i),(projectedData[i,0],projectedData[i,1]))
	#Plotting newly found data point
	if dataPoint:
		#dp = DataFrame(dataPoint).as_matrix() #new data point
		dp = dataPoint
		pdp = np.dot(dp,projectionVec)#projected new data point
		pl.annotate('Point of Interest',(pdp[:,0],pdp[:,1]))
	pl.show()

def get_nearest_sample_from_allclusters(clusters,orgDict,nonBots,excludeList):
	dist = {}
	for id in nonBots:
		if id not in excludeList and len(orgDict[id])>0:
			dp = np.array(orgDict[id]) #data point
			rep_dp = np.array([dp]*len(clusters))
			#dist = map(lambda x : np.linalg.norm(x-dp),clusters)						
			cluster_dists = map(lambda x : np.linalg.norm(x), clusters-rep_dp)		
			dist[id] = np.min(cluster_dists);
	userId = min(dist,key=dist.get)
	print 'Nearest Id to clusters is : ' + str(userId) + ' with dst : ' + str(dist[userId])
	return userId

def get_bot_specific_data(botId,overallD):	
	newD = collections.defaultdict()	
	for id in botId:
		if len(overallD[id])>0:
			newD[id] = overallD[id]
	print 'Tweeted bots / Filtered data : ' + str(len(newD)) + ' out of ' + str(len(overallD))
	return newD

if __name__ == '__main__':
	detectTopK = 50
	botId = get_bots()

	userTopics = get_user_topic_rohit()
	nonBots = [x for x in userTopics.keys() if x not in botId]
	userTopic = get_bot_specific_data(botId,userTopics)
	#topicWord = get_topic_word_distr_rohit()	
	
	#Cluster bots and check which ids of nonbots are closest to some bot cluster in some appropriate space		
	#Visualizing next nearest points
	M = DataFrame(userTopic).fillna(0.00001).as_matrix().transpose()
	clusterId, clusters = cluster_data(M)
	plot_clusters_with_newdatapoints(clusters,clusterId,M,[],'UserInTopicSpace')
	excludeList = []
	for id in range(detectTopK):
		nearestKey = get_nearest_sample_from_allclusters(clusters,userTopics,nonBots,excludeList)	
		excludeList.append(nearestKey)	
	del userTopics, userTopic, clusters, clusterId, excludeList

	#userWords = get_user_word_rohit()
	#with open(userWordFile, 'r') as infile:
		userWords = json.load(infile)
	userWord = get_bot_specific_data(botId,userWords)
	#corpus = gensim.matutils.Dense2Corpus(numpy_matrix), #corpus = gensim.matutils.Sparse2Corpus(scipy_sparse_matrix), #scipy_csc_matrix = gensim.matutils.corpus2csc(corpus)
	#npM = gensim.matutils.corpus2dense(userWord)
	#print npM.shape
	#clusterId, clusters = cluster_data(npM)
	#plot_clusters_with_newdatapoints(clusters,clusterId,M,[],'UserInTopicSpace')
	#excludeList = []
	#for id in range(detectTopK):
	#	nearestKey = get_nearest_sample_from_allclusters(clusters,userTopics,nonBots,excludeList)	
	#	excludeList.append(nearestKey)	

		
	#plot_clusters_with_newdatapoints(clusters,clusterId,M,M[nearestKey],'UserInTopicSpace')
	#Visualizing topicInUserSpace	
	#clusterId, clusters = cluster_data(userTopic)
	#plot_clusters_with_newdatapoints(clusters,clusterId,userTopic,[],'TopicInUserSpace')
	#nearestKey = get_nearest_sample(clusters,userTopic,nonBots)
	#plot_clusters_with_newdatapoints(clusters,clusterId,userTopic,userTopic[nearestKey],'TopicInUserSpace')
