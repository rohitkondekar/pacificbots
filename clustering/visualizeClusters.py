from matplotlib.mlab import PCA
import numpy as np
import pylab as pl
import graphlab as gl
import sklearn.cluster as skc
import sklearn.decomposition as skd

class Base(object):
	def __init__(self, labelStr = "None"):
		self.dim1 = 200
		self.dim2 = 100
		self.topK = 2  #Projection dimension
		self.inpDir = "/home/mahesh_t/tweetAnalyzer/data/rohit_predictions"
		self.labelStr = labelStr
	def cluster_reassign(self):
		tmp = range(len(self.clusterId))
		for idx in range(len(self.clusterId)):
			tmp[idx] = self.clusterId[idx].argmax()
		self.clusterId = np.array(tmp)
	def get_random_data(self):
		self.data = np.array(np.random.randint(1,4,size=(self.dim1,self.dim2)))
		return self.data
	def get_data_from_sframe(self,flag=0): #Populates self.data
		self.data = gl.load_sframe(self.inpDir+"/pred/").to_dataframe().as_matrix()	
		if flag:
			self.data = self.data.transpose()
		self.dim1 = len(self.data)
		self.dim2 = len(self.data[0])		
		#self.clusterId = gl.load_sframe(self.inpDir+"/topic_pred/").to_dataframe().as_matrix()
		#self.cluster_reassign() #Remove this once the same data issue is fixed
		#return self.data, self.clusterId
		return self.data
	def project(self,projectionVec):
		self.projectedData = np.dot(self.data,projectionVec) #Use multiply for np.matrix
		return self.projectedData
	def plot(self,labelStr):	
		pl.title(labelStr)
		s = pl.scatter(self.projectedData[:,0], self.projectedData[:,1], s=50, c = (1.0*self.clusterId)/self.dim2)
		pl.gcf().colorbar(s).set_label('ColorBar : 0 corresponds to health topic')
		for i in range(len(self.projectedData[:,0])):
			#pl.annotate(str(self.clusterId[i]),(self.projectedData[i,0],self.projectedData[i,1]))
			pl.annotate(str(i),(self.projectedData[i,0],self.projectedData[i,1]))
		pl.show()
	def cluster(self):
		num_clusters = min(self.dim1,self.dim2)
		if num_clusters==self.dim1:
			model = skc.KMeans(n_clusters=num_clusters, init=self.data, precompute_distances=True, n_jobs=10)	
		else:
			model = skc.KMeans(n_clusters=num_clusters, precompute_distances=True, n_jobs=10)
		self.clusterId = model.fit_predict(self.data)
		return model.cluster_centers_
	def get_projection_vec(self,clusterData):
		u, s, v = np.linalg.svd(clusterData)
		#idx = (-np.array(abs(val))).argsort()[:2] 
		self.projectionVec = v[:,0:self.topK]
		return self.projectionVec
	def doWork(self):
		clusterData = self.cluster()
		self.project(self.get_projection_vec(clusterData))
		self.plot(self.labelStr)

class ClusterTopic(Base): #Cluster topic bassed on user vector
	def __init__(self):
		Base.__init__(self,"Cluster topics in user space")		
		super(ClusterTopic,self).get_data_from_sframe(1)
	def doWork(self):		
		super(ClusterTopic,self).doWork()

class ClusterUser(Base): #Cluster user based on topic vectors
	def __init__(self):
		Base.__init__(self,"Cluster users in topic space")
		super(ClusterUser,self).get_data_from_sframe(0) 
	def	doWork(self):
		super(ClusterUser,self).doWork()	

if __name__ == '__main__':
	ClusterUser().doWork()
	ClusterTopic().doWork()
