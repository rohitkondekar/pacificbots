import os
import sys
import urllib2
import json
import graphlab as gl
import logging

input  = "./data/tweets"
output = "./data/tweetTable"

f = open(input)
g = gl.SGraph()

dictTweets = dict()

for line in f:
	try:
		tweet = json.loads(line)
		dictTweets.setdefault('user_id',[]).append(tweet['user_id'])
		dictTweets.setdefault('text',[]).append(tweet['text'])
		dictTweets.setdefault('created_at',[]).append(tweet['created_at'])
		dictTweets.setdefault('id_str',[]).append(tweet['id_str'])
	except:
		print "Issue at : ", str(tweet)
		continue

gtable = gl.SFrame(dictTweets)
gtable.save(output)
