import os
import sys
import urllib2
import json
import graphlab as gl


##########################################
### Graphlab TopicModelling code
### Read the dictionary and feed in as data frame
### Remove Stop words and rt tag
### AliasLDA model - alpha = 50/numberoftopics, Beta = 0.01
##########################################

def topicModelling(dictPath="/home/mahesh_t/DR/data/subtweets/userFinalDictionary",meth="alias"):

	userTweets = dict()
	with open(dictPath,'r') as r_file:
		userTweets = json.load(r_file)

	users = userTweets.keys()
	tweets = userTweets.values()

	gframe = gl.SFrame({'user':users,'tweets':tweets})

	#Create Bag of Words
	tweets = gl.text_analytics.count_words(gframe['tweets'])
	#Remove Stop Words
	stopwords = gl.text_analytics.stopwords() | {'rt'} | {'RT'} | {'rT'} | {'Rt'}
	tweets = tweets.dict_trim_by_keys(stopwords, exclude=True)
	# Learn topic model
	#alias or cgs
	model = gl.topic_model.create(tweets,method=meth,beta=0.01,num_topics=10,num_iterations=10)
	pred = model.predict(tweets)

	print(model.get_topics(output_type='topic_words'))
	print(model.get_topics())
	print(model)
	print(model['topics'])
	print("Pred length: "+str(len(pred)))
	print(type(pred))

	pred.save("data/testfile",'text')
	#with open("testfile",'w') as w_file:
	 	
	
	# print(tweets[:2])
	# print(len(model['vocabulary']))
	# print(model.predict(tweets))


	# input("Press Enter to continue...")

def main(args):
	if len(args)>1:
		topicModelling(args[1])
	else:
		topicModelling()


if __name__ == '__main__':
	# change worker limit to 8
	gl.set_runtime_config('GRAPHLAB_DEFAULT_NUM_PYLAMBDA_WORKERS', 6)
	gl.set_runtime_config('GRAPHLAB_DEFAULT_NUM_GRAPH_LAMBDA_WORKERS', 6)
	# verify
	print(gl.get_runtime_config())
	main(sys.argv)
