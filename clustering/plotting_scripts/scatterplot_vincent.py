import vincent
import pandas as pd
import random
from IPython.core.display import display, HTML
from json2html import *

vincent.core.initialize_notebook()

cat_2 = ['y' + str(x) for x in range(0, 10, 1)]
index_2 = range(1, 21, 1)
multi_iter2 = {'index': index_2}
for cat in cat_2:
    multi_iter2[cat] = [random.randint(10, 100) for x in index_2]

#As DataFrames
index_3 = multi_iter2.pop('index')
df_1 = pd.DataFrame(multi_iter2, index=index_3)
df_1 = df_1.reindex(columns=sorted(df_1.columns))

scatter = vincent.Scatter(df_1)
scatter.axis_titles(x='Index', y='Data Value')
scatter.legend(title='Categories')
scatter.colors(brew='Set3')

#scatter.display()
#open('scatter_vincent.html','w').write(json2html.convert(json=scatter.to_json()))
