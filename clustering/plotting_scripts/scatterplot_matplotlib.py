import matplotlib.pyplot as plt
import numpy as np

x=[1,2,3,4,5]
y=[2,3,4,5,6]
s=['sf','sfg','q','w','e']
f = lambda x,y : np.sqrt(x**2+y**2)
colors = map(f,x,y)
plt.scatter(x,y, c=colors, alpha=0.5)
plt.title('abc')

for i, txt in enumerate(s):
    plt.annotate(txt, (x[i],y[i]))

plt.show()
