from matplotlib.mlab import PCA
import numpy as np
import pylab as pl
import graphlab as gl
import sklearn.cluster as skc
import sklearn.decomposition as skd

class Projection(object):
	def __init__(self, labelStr = "None"):
		self.dim1 = 200
		self.dim2 = 100
		self.topK = 2  #Projection dimension
		self.inpDir = "/home/mahesh_t/tweetAnalyzer/data/rohit_predictions"
		self.labelStr = labelStr
	def cluster_reassign(self):
		tmp = range(len(self.clusterId))
		for idx in range(len(self.clusterId)):
			tmp[idx] = self.clusterId[idx].argmax()
		self.clusterId = np.array(tmp)
	def get_random_data(self):
		self.data = np.array(np.random.randint(1,4,size=(self.dim1,self.dim2)))
		return self.data
	def get_data_from_sframe(self): #Populates self.data
		self.data = gl.load_sframe(self.inpDir+"/pred/").to_dataframe().as_matrix()	
		self.dim1 = len(self.data)
		self.dim2 = len(self.data[0])
		self.clusterId = gl.load_sframe(self.inpDir+"/topic_pred/").to_dataframe().as_matrix()
		self.cluster_reassign() #Remove this once the same data issue is fixed
		return self.data, self.clusterId
	def project(self,projectionVec):
		self.projectedData = np.dot(self.data,projectionVec) #Use multiply for np.matrix
		return self.projectedData
	def get_projection_vec(self):
		pass
	def plot(self,labelStr):		
		pl.title(labelStr)
		s = pl.scatter(self.projectedData[:,0], self.projectedData[:,1], s=50, c = (1.0*self.clusterId)/self.dim2)
		pl.gcf().colorbar(s).set_label('ColorBar : 0 corresponds to health topic')
		pl.show()	
	def doWork(self,projectionVec):
		self.project(projectionVec) #Populates self.projectedData
		self.plot(self.labelStr)	

class VizTopicUser(Projection): #Visualize topic in user space
	def __init__(self):
		Projection.__init__(self,"Topics in user space")		
		super(VizTopicUser,self).get_data_from_sframe() 
	def get_projection_vec(self):
		u,s,v = np.linalg.svd(self.data)
		self.projectionVec = v[:,0:self.topK]  #noTopics x topK matrix
		return self.projectionVec
	def doWork(self):
		super(VizTopicUser,self).doWork(self.get_projection_vec())

class VizTopics(Projection): #Visualize topic vectors
	def __init__(self):
		Projection.__init__(self,"Topic vectors")
		super(VizTopics,self).get_data_from_sframe() 
	def get_projection_vec(self):
		u,s,v = np.linalg.svd(self.data)
		self.projectionVec = v[0:self.topK,:].transpose()  #topK x noTopics matrix	
		return self.projectionVec	
	def	doWork(self):
		super(VizTopics,self).doWork(self.get_projection_vec())		

class VizUsers(Projection): #Visualize user vectors
	def __init__(self):
		Projection.__init__(self,"Topic vectors")
		super(VizTopics,self).get_data_from_sframe() 
	def get_projection_vec(self):
		u,s,v = np.linalg.svd(self.data)
		self.projectionVec = u[:,0:topK].transpose()  
		return self.projectionVec	
	def	doWork(self):
		super(VizTopics,self).doWork(self.get_projection_vec())	

if __name__ == '__main__':
	#VizTopics().doWork()
	VizTopicUser().doWork()
