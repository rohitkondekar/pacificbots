import multiprocessing
import os
import sys
import time
import re
import json
import urllib2
import logging
import os
import HTMLParser


##########################################
## Get title from URL in tweets if possible
## and store it in a dictionary
## Do this in parallel fasion
##########################################
def preProcessData(fileName):
	html_parser = HTMLParser.HTMLParser()
	pid = os.getpid()
	logging.basicConfig(filename='scrapping.log', level=logging.INFO)
	print(multiprocessing.current_process())
	userData = dict()
	pattern = re.compile(ur'(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?\xab\xbb\u201c\u201d\u2018\u2019]))')
	titleRE = re.compile('<title>(.*?)</title>', re.IGNORECASE|re.DOTALL)
	with open(fileName,'r') as r_file:
		linenum = 0
		for line in r_file:
			linenum+=1
			try:
				js = json.loads(line)
				userid = js['user_id']
				tweet = js['text']
				tweet = html_parser.unescape(tweet)
				tweet = re.sub(r'^RT',"",tweet)
				tweet = pattern.sub("",tweet)
				urls = js['entities']['urls']
				
				urls = list(set(urls))
				if "" in urls:
					urls.remove("")
				for url in urls:
					try:
						response = urllib2.urlopen(url,timeout=10)
						title = titleRE.search(response.read().decode('ISO-8859-1')).group(1);
						title = html_parser.unescape(pattern.sub("",title))
						title = re.sub('[.!~^*(),:;"\'<>`=+-\\/]',"",title)
						tweet += " mytweetseparatordelimitor "+title	
					except Exception as e:
						logging.info(e)
						continue
				if userid not in userData:
					userData[userid] = tweet
				else:
					userData[userid] += " "+tweet

				if linenum%20==0:
					logging.info('User database size for pid:'+ str(pid)+ ' = '+str(len(userData)))
			except Exception as e:
				logging.info(e)
				logging.info('There was a error in JSON at - '+str(linenum))
				continue
	
	with open(fileName+"_output",'w') as w_file:
		json.dump(userData,w_file)
				

##########################################
## Takes in initial file name as they are in sorted fasion 
## from where to start parsing. All files after that are parsed
##########################################

if __name__ == '__main__':

	folderName = "../data/subtweets/"
	fileNames = []
	for file in sorted(os.listdir("../data/subtweets")):
		fileNames.append(folderName+file)

	if len(sys.argv)>1:
		startTag = sys.argv[1] #filename to start parsing from
		fileNames = fileNames[fileNames.find(startTag):]

	pool = multiprocessing.Pool(processes=81)
	print(len(fileNames))
	pool.map(preProcessData, fileNames)
	
